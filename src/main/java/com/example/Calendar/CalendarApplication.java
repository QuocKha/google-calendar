package com.example.Calendar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@Controller
public class CalendarApplication extends SpringBootServletInitializer{

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application){
        return application.sources(CalendarApplication.class);
    }


    public static void main(String[] args) {
        SpringApplication.run(CalendarApplication.class, args);
    }

        @RequestMapping("/login")
        public String login() {
            return "google/login.html";
        }

    @RequestMapping("/infor")
    public String infor() {
        return "google/infor.html";
    }
}