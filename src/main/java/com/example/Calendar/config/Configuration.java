package com.example.Calendar.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceView;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

@org.springframework.context.annotation.Configuration
@EnableWebMvc
@ComponentScan
public class Configuration extends WebMvcConfigurerAdapter
{
//    @Override
//    public void configureViewResolvers(ViewResolverRegistry registry) {
//        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
//        resolver.setPrefix("/WEB-INF/view/");
//        resolver.setSuffix(".html");
//        registry.viewResolver(resolver);
//    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/google/**").addResourceLocations("classpath:/google/");
        super.addResourceHandlers(registry);

        registry.addResourceHandler("/css/**")
                .addResourceLocations("classpath:/google/css/");

        registry.addResourceHandler("/simple_picker/**")
                .addResourceLocations("classpath:/google/simple_picker/");

        registry.addResourceHandler("/images/**")
                .addResourceLocations("classpath:/google/images/");


        registry.addResourceHandler("/js/**")
                .addResourceLocations("classpath:/google/js/");

        registry.addResourceHandler("/simple_picker/**")
                .addResourceLocations("classpath:/google/simple_picker/");
    }

    @Bean
    public ViewResolver viewResolver() {
        UrlBasedViewResolver viewResolver = new UrlBasedViewResolver();
        viewResolver.setViewClass(InternalResourceView.class);
        return viewResolver;
    }
}


