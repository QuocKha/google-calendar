package com.example.Calendar.controller;

import javax.servlet.http.HttpServletRequest;

import com.example.Calendar.model.Calendarmodel;
import com.google.api.services.calendar.model.Acl;
import com.google.api.services.calendar.model.AclRule;
import com.google.api.services.calendar.model.EventDateTime;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

import com.google.api.client.auth.oauth2.AuthorizationCodeRequestUrl;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets.Details;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.Event;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Controller
public class GoogleController {

    private static final String COMMA_DELIMITER = ",";

    private final static Log logger = LogFactory.getLog(GoogleController.class);
    private static final List<String> SCOPES = Collections.singletonList(CalendarScopes.CALENDAR);
    private static final String APPLICATION_NAME = "";
    private static HttpTransport httpTransport;
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static com.google.api.services.calendar.Calendar client;

    GoogleClientSecrets clientSecrets;
    GoogleAuthorizationCodeFlow flow;
    Credential credential;

    @Value("${google.client.client-id}")
    private String clientId;
    @Value("${google.client.client-secret}")
    private String clientSecret;
    @Value("${google.client.redirectUri}")
    private String redirectURI;


    final DateTime date1 = new DateTime("2017-05-05T16:30:00.000+05:30");
    final DateTime date2 = new DateTime("2019-05-05T16:30:00.000+05:30");
    //DateTime(new Date());

    @RequestMapping(value = "/l", method = RequestMethod.GET)
    public String hello() {
        System.out.println("322342");
        return "hello";
    }

    @RequestMapping(value = "/login/google", method = RequestMethod.GET)
    public RedirectView googleConnectionStatus(HttpServletRequest request) throws Exception {
        return new RedirectView(authorize());
    }

    @RequestMapping(value = "/login/google", method = RequestMethod.GET, params = "code")
    public ResponseEntity<String> oauth2Callback(@RequestParam(value = "code") String code) {
        com.google.api.services.calendar.model.Events eventList;
        String message= "";
        try {
            TokenResponse response = flow.newTokenRequest(code).setRedirectUri(redirectURI).execute();
            credential = flow.createAndStoreCredential(response, "userID");
            client = new com.google.api.services.calendar.Calendar.Builder(httpTransport, JSON_FACTORY, credential)
                    .setApplicationName(APPLICATION_NAME).build();

            String pageToken = null;
            do {
                eventList = client.events().list("primary").setPageToken(pageToken).execute();
                List<Event> items = eventList.getItems();
                for (Event event : items) {
                    System.out.println(event.getSummary());

                    logger.info(event.getSummary());
                    client.events().delete("primary", event.getId()).execute();
                    logger.info("delete complete");

                }

                pageToken = eventList.getNextPageToken();
            } while (pageToken != null);

            System.out.println("My:" + eventList.getItems());

            // credentials.
            logger.info("-----------start insert calendar-----------");
            List<Calendarmodel> list = listCalendar("data/data.csv");
            if (list.isEmpty() || list.size() == 1) {
                logger.info("------------No read file csv----------");
                return new ResponseEntity<>("Error!", HttpStatus.BAD_REQUEST);
            }

            for (int i = 1; i < list.size(); i++) {

                Calendarmodel calendarmodel = list.get(i);
                message = message + calendarmodel.toString();
                Event event = new Event().setSummary(calendarmodel.getSubject());

                DateTime startDateTime = new DateTime(subString(calendarmodel.getStartdate()) + "T" + subString(calendarmodel.getStarttime()) + ":00+07:00");
                EventDateTime start = new EventDateTime()
                        .setDateTime(startDateTime)
                        .setTimeZone("UTC");
                event.setStart(start);

                DateTime endDateTime = new DateTime(subString(calendarmodel.getStartdate()) + "T" + subString(calendarmodel.getEndtime()) + ":00+07:00");
                EventDateTime end = new EventDateTime()
                        .setDateTime(endDateTime)
                        .setTimeZone("UTC");
                event.setEnd(end);

                String calendarId = "primary";
                event = client.events().insert(calendarId, event).execute();
                logger.info(event.getId());
                System.out.printf("Event created: %s\n", event.getHtmlLink());
            }

            // Iterate over a list of access rules

            Acl acl = client.acl().list("primary").execute();

            for (AclRule rules : acl.getItems()) {
                System.out.println(rules.getId() + ": " + rules.getRole());
            }

        } catch (Exception e) {
            logger.warn("Exception while handling OAuth2 callback (" + e.getMessage() + ")."
                    + " Redirecting to google connection status page.");
            message = "Exception while handling OAuth2 callback (" + e.getMessage() + ")."
                    + " Redirecting to google connection status page.";
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }

        System.out.println("cal message:" + message);

        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteCalendar() {
        try {
            com.google.api.services.calendar.model.Events eventList;

            List<Calendarmodel> list = listCalendar("data/datadelete.csv");

            if (list.isEmpty() || list.size() == 1) {
                return new ResponseEntity<>("Error!", HttpStatus.BAD_REQUEST);
            }
            String pageToken = null;
            for (int i = 1; i < list.size(); i++) {
                Calendarmodel calendarmodel = list.get(i);

                do {
                    eventList = client.events().list("primary").setPageToken(pageToken).execute();
                    List<Event> items = eventList.getItems();
                    for (Event event : items) {
                        logger.info(event.getSummary());
                        if(event.getSummary().equals(calendarmodel.getSubject())) {
                            client.events().delete("primary", event.getId()).execute();
                            logger.info("delete complete");
                        }
                    }

                    pageToken = eventList.getNextPageToken();
                } while (pageToken != null);

            }

        } catch (Exception e) {
            logger.warn("Exception while handling OAuth2 callback (" + e.getMessage() + ")."
                    + " Redirecting to google connection status page.");
        }

        return new ResponseEntity<>("Success", HttpStatus.OK);
    }


    @RequestMapping(value = "/update", method = RequestMethod.PUT, params = "id")
    public ResponseEntity<?> updateCalendar(@RequestParam(value = "id") String id) {
        try {
            com.google.api.services.calendar.model.Events eventList;

            String pageToken = null;
            do {
                eventList = client.events().list("primary").setPageToken(pageToken).execute();
                List<Event> items = eventList.getItems();
                for (Event event : items) {
                    System.out.println(event.getSummary());
                    if(event.getId().equals(id)){
                        // Make a change
                        event.setSummary("Appointment at Somewhere");

                        // Update the event
                        Event updatedEvent = client.events().update("primary", event.getId(), event).execute();

                        System.out.println(updatedEvent.getUpdated());
                    }

                }

                pageToken = eventList.getNextPageToken();
            } while (pageToken != null);


        } catch (Exception e) {
            logger.warn("Exception while handling OAuth2 callback (" + e.getMessage() + ")."
                    + " Redirecting to google connection status page .");
        }

        return new ResponseEntity<>("Success", HttpStatus.OK);
    }

    @RequestMapping(value = "/role", method = RequestMethod.POST)
    public ResponseEntity<?> setRole(@RequestParam(value = "mail", defaultValue = "khahq.citynow@gmail.com") String mail,
                                     @RequestParam(value = "role", defaultValue = "user") String role) {

        try {
            // Create access rule with associated scope
            AclRule rule = new AclRule();
            AclRule.Scope scope = new AclRule.Scope();
            scope.setType("user").setValue(mail);
            if(role.equals("admin")){
                rule.setScope(scope).setRole("owner");
                logger.info("-------------------admin---------------");
            } else {
                rule.setScope(scope).setRole("reader");
                logger.info("-----------------------manager------------------");
            }

            // Insert new access rule
            AclRule createdRule = client.acl().insert("primary", rule).execute();
            System.out.println(createdRule.getId());

        } catch (Exception e) {
            logger.warn("Exception while handling OAuth2 callback (" + e.getMessage() + ")."
                    + " Redirecting to google connection status page .");
        }
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }


    private String authorize() throws Exception {
        AuthorizationCodeRequestUrl authorizationUrl;

        if (flow == null) {
            Details web = new Details();
            web.setClientId(clientId);
            web.setClientSecret(clientSecret);
            clientSecrets = new GoogleClientSecrets().setWeb(web);
            httpTransport = GoogleNetHttpTransport.newTrustedTransport();
            flow = new GoogleAuthorizationCodeFlow.Builder(httpTransport, JSON_FACTORY, clientSecrets,
                    SCOPES).build();
        }
        authorizationUrl = flow.newAuthorizationUrl().setRedirectUri(redirectURI);
        logger.info("cal authorizationUrl->" + authorizationUrl);
        return authorizationUrl.build();
    }


    public List<Calendarmodel> listCalendar(String link) {
        BufferedReader br = null;
        List<Calendarmodel> list = new ArrayList<>();
        try {
            String line;
            br = new BufferedReader(new FileReader(link));

            // How to read file in java line by line?
            while ((line = br.readLine()) != null) {
                list.add(parseCsvLine(line));
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)
                    br.close();
            } catch (IOException crunchifyException) {
                crunchifyException.printStackTrace();
            }
        }

        return list;
    }

    public Calendarmodel parseCsvLine(String csvLine) {
        Calendarmodel calendarmodel = new Calendarmodel();
        if (csvLine != null) {
            String[] splitData = csvLine.split(COMMA_DELIMITER);
            calendarmodel.setSubject(splitData[0]);
            calendarmodel.setStartdate(splitData[1]);
            calendarmodel.setStarttime(splitData[2]);
            calendarmodel.setEndtime(splitData[3]);
        }
        return calendarmodel;
    }

    public String subString(String s) {
        return s.substring(1, s.length() - 1);
    }


}